# datasets_sts_paraphrase_xlm-roberta-base_de-en

All datasets created for training and testing of https://huggingface.co/PM-AI/sts_paraphrase_xlm-roberta-base_de-en

- all_cross_train_unique.csv: english + german mixed, STSb + SICK + Priya22 combined, deduplicated, training data
- all_cross_test_unique.csv: english + german mixed, STSb + SICK + Priya22 combined, deduplicated, test data

- all_de_train_unique.csv: german only, STSb + SICK + Priya22 combined, deduplicated, training data
- all_de_test_unique.csv: german only, STSb + SICK + Priya22 combined, deduplicated, test data

- all_en_train_unique.csv: english only, STSb + SICK + Priya22 combined, deduplicated, training data
- all_en_test_unique.csv: english only, STSb + SICK + Priya22 combined, deduplicated, test data

- priya22_cross_test_unique.csv: english + german mixed, Priya22 only, deduplicated, test data
- priya22_de_test_unique.csv: german only, Priya22 only, combined, deduplicated, test data
- priya22_en_test_unique.csv: english only, Priya22 only, deduplicated, test data

- sickr_cross_test_unique.csv: english + german mixed, SICK only, deduplicated, test data
- sickr_de_test_unique.csv: german only, SICK only, deduplicated, test data
- sickr_en_test_unique.csv: english only, SICK only, deduplicated, test data

- stsb_cross_test_unique.csv: english + german mixed, STSb only, deduplicated, test data
- stsb_de_test_unique.csv: german only, STSb only, deduplicated, test data
- stsb_en_test_unique.csv: english only, STSb only, deduplicated, test data

All training and test data (STSb, Sick, Priya22) were checked for duplicates within and with each other and removed if found. Because the test data is prioritized, duplicated entries between test-train are exclusively removed from train split. 

Origins of datasets:
- STSb: https://huggingface.co/datasets/stsb_multi_mt
  - trainin data is test + dev split, test data is test split
- SICK: https://huggingface.co/datasets/mteb/sickr-sts
  - random train-test split created in 80:20 ratio
  - translation for german created with DeepL
- Priya22: https://github.com/Priya22/semantic-textual-relatedness
  - random train-test split created in 80:20 ratio
  - translation for german created with DeepL

